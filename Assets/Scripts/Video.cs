using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using TMPro;

public class Video : MonoBehaviour
{
    public bool vumark = true;

    public VideoPlayer[] _videoPlayers;
    public GameObject[] videoGameObjects;
    public GameObject cloud;
    public Animator cloudAnim;
    public GameObject subs;
    public GameObject sub2;
    public TextMeshPro subsText;
    public TextMeshPro sub2Text;
    public MeshRenderer videoRender;
    public MeshRenderer videoRender2;

    //Frases of the 1 subs
    public string phrase1;
    public int phrase1Time;
    public string phrase2;
    public int phrase2Time;
    public string phrase3;
    public int phrase3Time;
    public string phrase4;
    public int phrase4Time;
    public string phrase5;
    public int phrase5Time;
    public string phrase6;
    public int phrase6Time;
    public string phrase7;
    public int phrase7Time;
    public string phrase8;
    public int phrase8Time;
    public string phrase9;
    public int phrase9Time;
    public string phrase10;
    public int phrase10Time;
    public string phrase11;

    //Frases of the 2 subs
    public string sub2Phrase1;
    public int sub2Phrase1Time;
    public string sub2Phrase2;
    public int sub2Phrase2Time;
    public string sub2Phrase3;
    public int sub2Phrase3Time;
    public string sub2Phrase4;
    public int sub2Phrase4Time;
    public string sub2Phrase5;
    public int sub2Phrase5Time;
    public string sub2Phrase6;
    public int sub2Phrase6Time;
    public string sub2Phrase7;
    public int sub2Phrase7Time;
    public string sub2Phrase8;
    public int sub2Phrase8Time;
    public string sub2Phrase9;
    public int sub2Phrase9Time;
    public string sub2Phrase10;
    public int sub2Phrase10Time;
    public string sub2Phrase11;

    long frameVideo1;
    long frameVideo2;

    void OnEnable()
    {
        //videoRender = videoGameObjects[0].GetComponent<MeshRenderer>();
        //_videoPlayers[0].Play();
        //subs.SetActive(true);

        //StartCoroutine(Start1Video());

        _videoPlayers[0].Play();
        
        _videoPlayers[0].started += VideoStart;
        _videoPlayers[1].started += VideoStart2;
        _videoPlayers[0].loopPointReached += VideoFinish;
        _videoPlayers[1].loopPointReached += VideoFinish2;

        if(vumark)
        {
            _videoPlayers[0].frame = frameVideo1;
            _videoPlayers[1].frame = frameVideo2;
        }
        
    }

    void OnDisable()
    {
        /*videoGameObjects[0].SetActive(true);
        videoGameObjects[1].SetActive(false);
        cloud.SetActive(false);
        subs.SetActive(false);*/
    
        _videoPlayers[0].started -= VideoStart;
        _videoPlayers[1].started -= VideoStart2;
        _videoPlayers[0].loopPointReached -= VideoFinish;
        _videoPlayers[1].loopPointReached -= VideoFinish2;

        videoRender.enabled = false;
        subs.SetActive(false);
    }
    
    void Update()
    {
        frameVideo1 = _videoPlayers[0].frame;
        frameVideo2 = _videoPlayers[1].frame;
    }

    void VideoStart(VideoPlayer vp)
    {
        _videoPlayers[0].Play();
        videoRender.enabled = true;
        subs.SetActive(true);
        StartCoroutine(Subtitles());
    }

    IEnumerator Start1Video()
    {
        yield return new WaitForSeconds(1.5f);
        
        videoRender.enabled = true;
        subs.SetActive(true);
        StartCoroutine(Subtitles());
    }

    IEnumerator Subtitles()
    {
        Debug.Log("start subtitles");
        subsText.text = phrase1;
        yield return new WaitForSeconds(phrase1Time);
        subsText.text = phrase2;
        yield return new WaitForSeconds(phrase2Time);
        subsText.text = phrase3;
        yield return new WaitForSeconds(phrase3Time);
        subsText.text = phrase4;
        yield return new WaitForSeconds(phrase4Time);
        subsText.text = phrase5;
        yield return new WaitForSeconds(phrase4Time);
        subsText.text = phrase6;
        yield return new WaitForSeconds(phrase5Time);
        subsText.text = phrase7;
        yield return new WaitForSeconds(phrase6Time);
        subsText.text = phrase8;
        yield return new WaitForSeconds(phrase8Time);
        subsText.text = phrase9;
        yield return new WaitForSeconds(phrase9Time);
        subsText.text = phrase10;
        yield return new WaitForSeconds(phrase10Time);
        subsText.text = phrase11;
    }

    

    void VideoFinish(VideoPlayer vp)
    {
        if(vumark)
        {
            StartCoroutine(Start2Video());
        }
        else
        {
            videoGameObjects[1].SetActive(true);
            videoRender.enabled = false;
        }        
    }

    IEnumerator Start2Video()
    {
        Debug.Log("acabado");
        
        subs.SetActive(false);
        //linea comentada provisional
        //cloud.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        videoGameObjects[0].SetActive(false);
        videoGameObjects[1].SetActive(true);
        if(sub2 != null)
        {
            sub2.SetActive(true);
            StartCoroutine(Subtitles2());
        }
    }

    void VideoStart2(VideoPlayer vp)
    {
        subs.SetActive(false);
        videoGameObjects[0].SetActive(false);
        videoRender2.enabled = true;
        if(sub2 != null)
        {
            sub2.SetActive(true);
            StartCoroutine(Subtitles2());
        }
    }

    IEnumerator Subtitles2()
    {
        Debug.Log("start subtitles");
        sub2Text.text = sub2Phrase1;
        yield return new WaitForSeconds(sub2Phrase1Time);
        sub2Text.text = sub2Phrase2;
        yield return new WaitForSeconds(sub2Phrase2Time);
        sub2Text.text = sub2Phrase3;
        yield return new WaitForSeconds(sub2Phrase3Time);
        sub2Text.text = sub2Phrase4;
        yield return new WaitForSeconds(sub2Phrase4Time);
        sub2Text.text = sub2Phrase5;
        yield return new WaitForSeconds(sub2Phrase4Time);
        sub2Text.text = sub2Phrase6;
        yield return new WaitForSeconds(sub2Phrase5Time);
        sub2Text.text = sub2Phrase7;
        yield return new WaitForSeconds(sub2Phrase6Time);
        sub2Text.text = sub2Phrase8;
        yield return new WaitForSeconds(sub2Phrase8Time);
        sub2Text.text = sub2Phrase9;
        yield return new WaitForSeconds(sub2Phrase9Time);
        sub2Text.text = sub2Phrase10;
        yield return new WaitForSeconds(sub2Phrase10Time);
        sub2Text.text = sub2Phrase11;
    }

    void VideoFinish2(VideoPlayer vp)
    {
        Debug.Log("acabado2");
        videoGameObjects[1].SetActive(false);
        videoGameObjects[0].SetActive(true);
        VideoStart(vp);
        sub2.SetActive(false);
        //videoRender.enabled = false;
        //_videoPlayers[0].Stop();
        //linea comentada provisional
        //cloud.SetActive(false);
    }
}
