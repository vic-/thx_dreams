using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashManager : MonoBehaviour
{
    public GameObject logo;
    public Animator tutoAnim;

    void Start()
    {
        logo.SetActive(true);
        StartCoroutine(StartTutorial());
    }

    IEnumerator StartTutorial()
    {
        yield return new WaitForSeconds(1f);
        ShowTutorial();
    }

    void ShowTutorial()
    {
        tutoAnim.SetTrigger("Show");
    }

    public void ChangeScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }
}
