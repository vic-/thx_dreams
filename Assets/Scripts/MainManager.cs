using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class MainManager : MonoBehaviour
{
    public GameObject scannerSprite;
    public Animator thxAnim;
    public Animator backAnim;

    private bool scannerScreen = false;

    public GameObject[] videos;
    public GameObject[] names;
    public Text _nameSelected;
    private int _videoSelected = -1;

    public GameObject[] videoComp;
    public VideoPlayer[] videoPlayers;

    public void Start()
    {
        videoPlayers = new VideoPlayer[videos.Length];
        videoComp = new GameObject[videos.Length];

        for (int i = 0; i < videos.Length; i++)
        {
            videoPlayers[i] = videos[i].GetComponent<VideoPlayer>();
            videoComp[i] = videos[i].transform.GetChild(0).gameObject;
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    public void Scanner()
    {
        if(!scannerScreen)
        {
            scannerSprite.SetActive(true);
            thxAnim.SetTrigger("Up");
            backAnim.SetTrigger("Show");
            scannerScreen = true;
        }
    }

    public void CloseScanner()
    {
        if(scannerScreen)
        {
            scannerSprite.SetActive(false);
            thxAnim.SetTrigger("Down");
            backAnim.SetTrigger("Hide");
            scannerScreen = false;
        }
    }

    public void SearchName(Text searchedName)
    {
        Debug.Log(searchedName.text);
        foreach (GameObject name in names)
        {
            //if(name.name == searchedName.text)
            if(name.name.ToLower().Contains(searchedName.text.ToLower()))
            {
                name.SetActive(true);
            }
            else
            {
                name.SetActive(false);
            }
        }

    }

    public void SelectName(int nameSelected)
    {
        foreach (GameObject name in names)
        {
            name.SetActive(false);
        }
        //names[nameSelected].SetActive(true);
        _nameSelected.text = names[nameSelected].name;
        _videoSelected = nameSelected;
    }

    public void ApplyFilters()
    {
        if(_videoSelected >= 0)
        {
            foreach (GameObject video in videos)
            {
                video.SetActive(false);
            }
            videos[_videoSelected].SetActive(true);
        }
    }

    public void ClearFilters()
    {
        _videoSelected = -1;
        foreach (GameObject video in videos)
        {
            video.SetActive(true);
        }
        foreach (GameObject name in names)
        {
            name.SetActive(true);
        }
        _nameSelected.text = "";
    }
    
    public void Play(int videoNum)
    {
        if(!videoPlayers[videoNum].isPlaying)
        {
            videoComp[videoNum].SetActive(false);
            videoPlayers[videoNum].Play();

            foreach (VideoPlayer vPlayers in videoPlayers)
            {
                if(vPlayers.name != videoPlayers[videoNum].name)
                {
                    vPlayers.Pause();
                }
            }

            foreach (GameObject vComp in videoComp)
            {
                if(vComp.name != videoComp[videoNum].name)
                {
                    vComp.SetActive(true);
                }
            }
        }  
        else
        {
            videoComp[videoNum].SetActive(true);
            videoPlayers[videoNum].Pause();
        }
    }
}
