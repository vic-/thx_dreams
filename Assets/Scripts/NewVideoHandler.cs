using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using TMPro;

public class NewVideoHandler : MonoBehaviour
{
    [SerializeField]private bool video2;
    [SerializeField]private VideoPlayer[] _videoPlayers;

    [SerializeField]private GameObject subs;
    [SerializeField]private GameObject sub2;
    
    [SerializeField]private TextMeshPro subsText;
    [SerializeField]private TextMeshPro sub2Text;

    [SerializeField]private MeshRenderer videoRender;
    [SerializeField]private MeshRenderer videoRender2;

    [SerializeField]private string[] phrases1;
    [SerializeField]private float[] phrases1Time;

    [SerializeField]private string[] phrases2;
    [SerializeField]private float[] phrases2Time;

    void OnEnable()
    {
        videoRender.enabled = false;
        videoRender2.enabled = false;

        subs.SetActive(false);
        sub2.SetActive(false);

        if(!video2)
        {
            _videoPlayers[0].Prepare();
        }
        else
        {
            _videoPlayers[1].Prepare();
        }
        
        _videoPlayers[0].prepareCompleted += VideoStart;
        _videoPlayers[1].prepareCompleted += VideoStart;
        _videoPlayers[0].loopPointReached += VideoFinish;
        _videoPlayers[1].loopPointReached += VideoFinish;        
    }

    void OnDisable()
    {    
        _videoPlayers[0].prepareCompleted -= VideoStart;
        _videoPlayers[1].prepareCompleted -= VideoStart;
        _videoPlayers[0].loopPointReached -= VideoFinish;
        _videoPlayers[1].loopPointReached -= VideoFinish;
    }

    IEnumerator Subtitles(string[] phrases, float[] phrasesTime ,TextMeshPro text)
    {
        for (int i = 0; i < phrases.Length; i++)
        {
            text.text = phrases[i];
            yield return new WaitForSeconds(phrasesTime[i]);
        }
    }

    void VideoStart(VideoPlayer vp)
    {
        if(!video2)
        {
            StartSelectedVideo(_videoPlayers[0], videoRender, subs);
            StartCoroutine(Subtitles(phrases1, phrases1Time, subsText));
        }
        else
        {
            StartSelectedVideo(_videoPlayers[1], videoRender2, sub2);
            StartCoroutine(Subtitles(phrases2, phrases2Time, sub2Text));
        }
    }

    void StartSelectedVideo(VideoPlayer videoPlayer, MeshRenderer renderer, GameObject sub)
    {
        videoPlayer.Play();
        renderer.enabled = true;
        sub.SetActive(true);
    }

    void VideoFinish(VideoPlayer vp)
    {
        if(!video2)
        {
            FinishSelectedVideo(_videoPlayers[1], videoRender, subs);
        }
        else
        {
            FinishSelectedVideo(_videoPlayers[0], videoRender2, sub2);
        }
    }

    void FinishSelectedVideo(VideoPlayer videoPlayer, MeshRenderer renderer, GameObject sub)
    {
        video2 = !video2;

        sub.SetActive(false);
        renderer.enabled = false;

        videoPlayer.Prepare();
    }
}